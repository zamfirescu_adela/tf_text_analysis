const path = require("path");
const { spawn } = require("child_process");

/**
 * Run python script, pass in `-u` to not buffer console output
 * @return {ChildProcess}
 */
function runScript() {
  return spawn("python", [
    "-u",
    path.join(__dirname, "compute_input.py"),
    "--foo",
    "some value for foo",
  ]);
}

const subprocess = runScript();

// print output of script
subprocess.stdout.on("data", (data) => {
  console.log(`data:${data}`);
});
subprocess.stderr.on("data", (data) => {
  console.log(`error:${data}`);
});
subprocess.on("close", () => {
  console.log("Closed");
});
// const { spawn } = require("child_process");
// //const childPython = spawn("python", ["../py_version/keywordBert.py", "ooo"]);

// const childPython = spawn("python", ["../py_version/keywordBert.py"]);

// childPython.stdout.on("data", (data) => {
//   console.log(`stdout: ${data}`);
// });

// childPython.stderr.on("data", (data) => {
//   console.error(`stderr: ${data}`);
// });

// childPython.on("close", (code) => {
//   console.error(`exited with code: ${code}`);
// });
