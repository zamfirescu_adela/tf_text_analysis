//import { trigram } from "n-gram";

const stopwords = [
  "i",
  "me",
  "my",
  "myself",
  "we",
  "our",
  "ours",
  "ourselves",
  "you",
  "your",
  "yours",
  "yourself",
  "yourselves",
  "he",
  "him",
  "his",
  "himself",
  "she",
  "her",
  "hers",
  "herself",
  "it",
  "its",
  "itself",
  "they",
  "them",
  "their",
  "theirs",
  "themselves",
  "what",
  "which",
  "who",
  "whom",
  "this",
  "that",
  "these",
  "those",
  "am",
  "is",
  "are",
  "was",
  "were",
  "be",
  "been",
  "being",
  "have",
  "has",
  "had",
  "having",
  "do",
  "does",
  "did",
  "doing",
  "a",
  "an",
  "the",
  "and",
  "but",
  "if",
  "or",
  "because",
  "as",
  "until",
  "while",
  "of",
  "at",
  "by",
  "for",
  "with",
  "about",
  "against",
  "between",
  "into",
  "through",
  "during",
  "before",
  "after",
  "above",
  "below",
  "to",
  "from",
  "up",
  "down",
  "in",
  "out",
  "on",
  "off",
  "over",
  "under",
  "again",
  "further",
  "then",
  "once",
  "here",
  "there",
  "when",
  "where",
  "why",
  "how",
  "all",
  "any",
  "both",
  "each",
  "few",
  "more",
  "most",
  "other",
  "some",
  "such",
  "no",
  "nor",
  "not",
  "only",
  "own",
  "same",
  "so",
  "than",
  "too",
  "very",
  "s",
  "t",
  "can",
  "will",
  "just",
  "don",
  "should",
  "now",
];
module.exports = {
  jaccard: function (wordArr1, wordArr2) {
    const wordIntersection = wordArr1.filter((value) =>
      wordArr2.includes(value)
    );
    const wordUnion = [...new Set([...wordArr1, ...wordArr2])];
    let jIndex = (wordIntersection.length / wordUnion.length).toFixed(1);
    if (jIndex > 1) return 1;
    return jIndex;
  },
  splitSentences: function (largeText) {
    splitText = largeText.split(" ").flat();
    for (let i = 0; i < splitText.length; i++) {
      let lastChar = splitText[i].substr(splitText[i].length - 1);
      if (lastChar == "?" || lastChar == "." || lastChar == ",") {
        splitText[i] = splitText[i].substring(0, splitText[i].length - 1);
        splitText[i].toLowerCase();
      } else splitText[i].toLowerCase();
    }
    return splitText;
  },

  parseText: function (text) {
    //remove punctuation
    text = text.toString().replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, "");
    text = text.toString().replace(/\s{2,}/g, " ");

    //lower all words
    text = text.toLowerCase().split();

    //remove english stopwords
    text = text
      .toString()
      .replace(new RegExp("\\b(" + stopwords.join("|") + ")\\b", "g"), "");

    //remove extra whitespaces that were left from removing the stopwords
    text = text.replace(/\s{2,}/g, " ");

    return text;
  },
  generateNGrams(splitText) {
    //the npm library accepts an array with separated words
    //return trigram(splitText);
  },
  loadBertModel() {},
};
