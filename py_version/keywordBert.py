#print("Welcome ", str(sys.argv[1]))

import tensorflow as tf
import tensorflow_text as tf_text
import numpy as np
from utils import removeStopwords, parseText
from sentence_transformers import SentenceTransformer
from sklearn.metrics.pairwise import cosine_similarity

import numpy as np

def mmr(myDoc, top_n, diversity):

    doc=parseText(myDoc)
    doc=removeStopwords(doc)

    tokenizer = tf_text.WhitespaceTokenizer()
    tokens = tokenizer.tokenize(doc)


    candidates=tf.strings.ngrams(tokens, 3).numpy().tolist()
    candidates=[x.decode('utf-8') for x in candidates]

    model = SentenceTransformer('distilbert-base-nli-mean-tokens')
    doc_embedding = model.encode([doc])
    candidate_embeddings = model.encode(candidates)
    word_embeddings=candidate_embeddings
    words=candidates

    # Extract similarity within words, and between words and the document
    word_doc_similarity = cosine_similarity(word_embeddings, doc_embedding)
    word_similarity = cosine_similarity(word_embeddings)

    # Initialize candidates and already choose best keyword/keyphras
    keywords_idx = [np.argmax(word_doc_similarity)]
    candidates_idx = [i for i in range(len(words)) if i != keywords_idx[0]]

    for _ in range(top_n - 1):
        # Extract similarities within candidates and
        # between candidates and selected keywords/phrases
        candidate_similarities = word_doc_similarity[candidates_idx, :]
        target_similarities = np.max(word_similarity[candidates_idx][:, keywords_idx], axis=1)

        # Calculate MMR
        mmr = (1-diversity) * candidate_similarities - diversity * target_similarities.reshape(-1, 1)
        mmr_idx = candidates_idx[np.argmax(mmr)]

        # Update keywords & candidates
        keywords_idx.append(mmr_idx)
        candidates_idx.remove(mmr_idx)



    return [words[idx] for idx in keywords_idx]
