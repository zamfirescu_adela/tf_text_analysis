#.\env\Scripts\activate

from flask import Flask, jsonify, request, Response
from flask_cors import CORS, cross_origin

from utils import *
from keywordBert import mmr
from nltk import ngrams, FreqDist

import sys
import json
from bson import ObjectId
from json import JSONEncoder

with open('./speeches.json', encoding="utf8") as f:
  speechList = json.load(f)

DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'

CORS(app, resources={r'/*': {'origins': '*'}})

class MyEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__


@app.route('/results', methods=['GET', 'POST'])
def results():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        data_received=post_data.get('data')
        selectedTitle=data_received.get('selectedSpeech')


        selectedSpeech=next((x['text'] for x in speechList if x['title'] == selectedTitle), None)
        publishingLocation=next((x['publishing_location'] for x in speechList if x['title'] == selectedTitle), None)
        date_published=next((x['date_published'] for x in speechList if x['title'] == selectedTitle), None)
        url=next((x['url'] for x in speechList if x['title'] == selectedTitle), None)

        keywordList=mmr(selectedSpeech,  top_n=10, diversity=0.5)
        questionList=runSentenceClassifier(selectedSpeech)
        allSentences=stringToSentences(selectedSpeech)
        wordFreq=getWordFrequency(selectedSpeech)
        response_object['publishing_location'] = publishingLocation
        response_object['date_published'] = date_published
        response_object['url'] = url

        response_object['keywordList'] = keywordList
        response_object['questionList'] = questionList
        response_object['wordFreq'] = wordFreq
        response_object['totalSentences'] = len(allSentences)

    else:
        response_object['message'] = 'You called this route'
    return jsonify(response_object)

@app.route('/comparison', methods=['GET', 'POST'])
def comparison():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        selectedTitle=post_data.get('selectedSpeech')
        comparisonTitle=post_data.get('selectedSpeechComparison')
        speech1=next((x['text'] for x in speechList if x['title'] == comparisonTitle), None)
        speech2=next((x['text'] for x in speechList if x['title'] == selectedTitle), None)

        speech1= parseText(speech1)
        speech2= parseText(speech2)
        jaccardCoef=jaccard_coef(speech1, speech2)
        print("AAAAAAAAAAA")
        print(jaccardCoef)
        commonWordsList=commonWordsFound(speech1, speech2)
        print(commonWordsList)
        response_object['commonWords'] = commonWordsList
        response_object['similarity'] = jaccardCoef
        response_object['comparisonSpeech1'] = speech1
        response_object['comparisonSpeech2'] = speech2
        print("Speech was sent")

    else:
        response_object['message'] = 'You called this route'
    return jsonify(response_object)

if __name__ == '__main__':
    app.run()
