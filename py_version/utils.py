#.\env\Scripts\activate
from nltk.corpus import stopwords
from transformers import DistilBertTokenizerFast
from transformers import TFDistilBertForSequenceClassification

import tensorflow as tf
import nltk.data
import re

import string

#nltk.download()

tokenizer_sent = nltk.data.load('tokenizers/punkt/english.pickle')
tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')
loaded_model = TFDistilBertForSequenceClassification.from_pretrained("./model_question")

def jaccard_coef(wordArr1, wordArr2):
  words_doc1 = set(wordArr1.split(' '))
  words_doc2 = set(wordArr2.split(' '))

  intersection = words_doc1.intersection(words_doc2)
  union = words_doc1.union(words_doc2)

  return round(float(len(intersection)) / len(union), 2)

def commonWordsFound(wordArr1, wordArr2):
  words_doc1 = set(wordArr1.split(' '))
  words_doc2 = set(wordArr2.split(' '))

  intersection = words_doc1.intersection(words_doc2)
  intersection=list(intersection)
  return intersection

def stringToSentences(text):
  return tokenizer_sent.tokenize(text)

def parseText(largeText):
  words = largeText.split()
  # remove punctuation from each word
  words = [word.lower() for word in words]
  table = str.maketrans('', '', string.punctuation)
  stripped = [w.translate(table) for w in words]
  stripped = " ".join(stripped)
  return stripped

def removeStopwords(largeText):
  #remove english stopwords
  stops = set(stopwords.words("english"))
  words=largeText.split()
  largeText=[word for word in words if word not in stops]
  print(largeText)
  largeText = " ".join(largeText)
  print("2 - removeStopwords")

  return largeText

def getWordFrequency(largeText):
  myText=parseText(largeText)
  myText=removeStopwords(myText)
  print("3 - getWordFrequency")
  print(myText)

  tokens = nltk.word_tokenize(myText)
  fdist1 = nltk.FreqDist(tokens)

  filtered_word_freq = dict((word, freq) for word, freq in fdist1.items() if not word.isdigit())

  return filtered_word_freq


def runSentenceClassifier(text):
  my_objects=[]
  result=[]
  sentenceArr=stringToSentences(text)
  for sent in sentenceArr:
    print(sent)
    predict_input = tokenizer.encode(sent,
                                  truncation=True,
                                  padding=True,
                                  return_tensors="tf")

    tf_output = loaded_model.predict(predict_input)[0]

    tf_prediction = tf.nn.softmax(tf_output, axis=1).numpy()[0]

    probab= round(tf_prediction[1], 2)
    my_objects.append([sent, probab])

  for item in my_objects:
   if(item[1] > 0.8):
     result.append(item[0])


  #   probab= round(tf_prediction[1], 2)
  #   my_objects.append(SentenceObj(sent, probab))

  # for item in my_objects:
  #     if(item.probab > 0.8):
  #       result.append(item)

  # for item in result:
  #   print(item.sentence)

  #print(tf_output)

  return result;



doc="Fans, for the past two weeks you have been reading about a bad break I got. Yet today I consider myself the luckiest man on the face of the earth. I have been in ballparks for seventeen years and have never received anything but kindness and encouragement from you fans. Look at these grand men. Which of you wouldn’t consider it the highlight of his career just to associate with them for even one day? Sure I’m lucky. Who wouldn’t consider it an honor to have known Jacob Ruppert? Also, the builder of baseball’s greatest empire, Ed Barrow? To have spent six years with that wonderful little fellow, Miller Huggins? Then to have spent the next nine years with that outstanding leader, that smart student of psychology, the best manager in baseball today, Joe McCarthy? Sure I’m lucky. When the New York Giants, a team you would give your right arm to beat, and vice versa, sends you a gift - that’s something. When everybody down to the groundskeepers and those boys in white coats remember you with trophies -- that’s something. When you have a wonderful mother-in-law who takes sides with you in squabbles with her own daughter -- that’s something. When you have a father and a mother who work all their lives so you can have an education and build your body -- it’s a blessing. When you have a wife who has been a tower of strength and shown more courage than you dreamed existed -- that’s the finest I know. So, I close in saying that I might have been given a bad break, but I've got an awful lot to live for."
# doc=parseText(doc)
# doc=removeStopwords(doc)

doc=getWordFrequency(doc)
print(doc)
